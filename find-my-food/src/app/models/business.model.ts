export interface Business {
  rating?: number,
  categories?: any,
  name?: string,
  url?: string,
  coordinates?: object,
  image_url?: string,
  location?: object
}
