import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {SearchComponent} from './search/search.component';
import {ResultsComponent} from './results/results.component';
import {SearchService} from './services/search.service';

import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";

import {reducers, effects} from "./store";


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot({}), //Need this to prevent ReducerManager error see: https://github.com/ngrx/platform/issues/295
    StoreModule.forFeature('app-search', reducers),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature(effects)
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
