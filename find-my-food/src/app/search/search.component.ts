import {Component, OnInit} from '@angular/core';
import {SearchService} from "../services/search.service";


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  preferences:any = ['Pizza'];
  location:any = 'Lombokstraat 13,Utrecht,3531RA,The Netherlands';
  post:any;
  geolocationPosition:any;
  loading = false;
  suggestedFoods = ['Pizza', 'Turks', 'Vis', 'Tapas', 'Surinaams', 'Italiaans', 'Vegan', 'BBQ', 'Salade', 'Vegetarisch', 'Hollands', 'Snacks', 'Hamburgers', 'Wijnbar', 'Vietnamees', 'Chinees', 'Japans'];

  constructor(private searchService: SearchService) {
  }

  ngOnInit() {
    this.getResults();
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.geolocationPosition = position
        },
        error => {
          switch (error.code) {
            case 1:
              console.log('Permission Denied');
              break;
            case 2:
              console.log('Position Unavailable');
              break;
            case 3:
              console.log('Timeout');
              break;
          }
        }
      );
    }
  }

  onSubmit() {
    this.getResults();
  }

  onClick(preference:string) {
    this.preferences = preference;
    this.getResults();
  }

  toggleCategory(cat:string) {
    if (this.preferences.includes(cat)) {
      this.preferences = this.preferences.filter((f:any) => f !== cat);
    } else {
      if (this.preferencesIsArray()) {
        this.preferences = [...this.preferences, cat];
      } else {
        let currentPreferencesString = this.preferences.toString();
        currentPreferencesString = currentPreferencesString[0].toUpperCase() + currentPreferencesString.substring(1);
        this.preferences = [];
        this.preferences.push(currentPreferencesString,cat);
      }
      console.log(typeof this.preferences);
    }
    this.getResults();
  }

  preferencesIsArray() {
    return Array.isArray(this.preferences);
  }

  getResults() {
    let locationString = '';
    let preferencesString = '';
    if(typeof this.location === "object" && this.location.coords.latitude && this.location.coords.longitude) {
      locationString = `&latitude=${this.location.coords.latitude}&longitude=${this.location.coords.longitude}`;
    } else {
      //encode string to prevent errors when using Ã«Ã©Ã¤ in location
      locationString = `&location=${encodeURIComponent(this.location)}`;
    }
    if(Array.isArray(this.preferences)) {
      this.preferences.forEach((preference)=> {
        preferencesString += ' ' + preference;
      });
    } else {
      preferencesString = this.preferences;
    }


    this.searchService.getResults(this.preferences, locationString).subscribe(response => {
      console.log(response);
      this.post = response;
      this.loading = false;
    });
  }

}
