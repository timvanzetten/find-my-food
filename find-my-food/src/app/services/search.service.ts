import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class SearchService {


  headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Authorization': 'Bearer U3n3nXrkJpzZ3T8qX2N0NTgovy9xiqXgwk1Eu_CM40AmQB7j6Eml2zfRCMhQq4qPhIjWSnIwwEpvpIpatna7UBKbdmANBGbL6Ce-IfyUspWubHcAZazuQRDizswLW3Yx',
  }

  requestOptions = {
    headers: new HttpHeaders(this.headerDict),
  };

  constructor(private http: HttpClient) {
    this.http = http;
  }

  getResults(preferences: string, location: string) {
    let baseUrl = 'http://localhost:4200/proxy/search?term=';
    return this.http.get(baseUrl + `${encodeURIComponent(preferences)}${location}&locale=nl_NL&sort_by=distance`, this.requestOptions);
  }
}
