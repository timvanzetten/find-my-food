import {Injectable} from "@angular/core";
import {Effect, Actions} from "@ngrx/effects";
import {catchError, map, switchMap} from "rxjs/operators";

import * as businessActions from '../actions/businesses.action';
import * as fromServices from '../../services';
import {of} from "rxjs/observable/of";

@Injectable()
export class BusinessesEffects {
  constructor(private actions$: Actions, private businessService: fromServices.SearchService) {
  }

  @Effect()
  loadBusinesses$ = this.actions$
    .ofType(businessActions.LOAD_BUSINESSES)
    .pipe(switchMap(() => {
        return this.businessService.getResults().pipe(
          map(businesses => new businessActions.LoadBusinessesSuccess(businesses)),
          catchError(error => of (new businessActions.LoadBusinessesFail(error)))
        )
      })
    );
}
