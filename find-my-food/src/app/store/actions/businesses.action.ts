import {Action} from '@ngrx/store';
import {Business} from '../../models/business.model';

export const LOAD_BUSINESSES = '[Businesses] Load Businesses';
export const LOAD_BUSINESSES_FAIL = '[Businesses] Load Businesses Fail';
export const LOAD_BUSINESSES_SUCCESS = '[Businesses] Load Businesses Fail';

export class LoadBusinesses implements Action{
  readonly type = LOAD_BUSINESSES;
}

export class LoadBusinessesFail implements Action{
  readonly type = LOAD_BUSINESSES_FAIL;
  constructor(public payload: any) {}
}

export class LoadBusinessesSuccess implements Action{
  readonly type = LOAD_BUSINESSES_SUCCESS;
  constructor(public payload: Business[]) {}
}

//action types
export type BusinessesAction = LoadBusinesses | LoadBusinessesFail | LoadBusinessesSuccess;
