import * as fromBusiness from '../actions/businesses.action';
import {Business} from "../../models/business.model";

export interface BusinessState {
  data: Business[],
  loaded: boolean,
  loading: boolean
}

export const initialState: BusinessState = {
  data: [],
  loaded: false,
  loading: false
};

export function reducer(
  state = initialState,
  action: fromBusiness.BusinessesAction
): BusinessState {
  switch (action.type) {
    case fromBusiness.LOAD_BUSINESSES: {
      return {
        ...state,
        loading: true,
      }
    }
    case fromBusiness.LOAD_BUSINESSES_SUCCESS: {
      const data = action.payload.businesses;
      return {
        ...state,
        loading: false,
        loaded: true,
        data
      }
    }
    case fromBusiness.LOAD_BUSINESSES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      }
    }
  }
  return state;
}

export const getBusinessesLoading = (state: BusinessState) => state.loading;
export const getBusinessesLoaded = (state: BusinessState) => state.loading;
export const getBusinesses = (state: BusinessState) => state.data;
