import * as fromBusiness from './business.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {getBusinesses} from "./business.reducer";

export interface SearchState {
  businesses: fromBusiness.BusinessState
}

export const reducers: ActionReducerMap<SearchState> = {
  businesses: fromBusiness.reducer
};

export const getSearchState = createFeatureSelector<SearchState>('app-search');

// businesses state
export const getBusinessesState = createSelector(getSearchState, (state: SearchState) => state.businesses);

export const getAllBusinesses = createSelector(getBusinessesState, fromBusiness.getBusinesses);
export const getBusinessesLoading = createSelector(getBusinessesState, fromBusiness.getBusinessesLoading);
export const getBusinessesLoaded = createSelector(getBusinessesState, fromBusiness.getBusinessesLoaded);

